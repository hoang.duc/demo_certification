package main

import (
	"fmt"
	u "myapp/pdfGenerator"
)

func main() {

	r := u.NewRequestPdf("")

	//html template path
	templatePath := "templates/template_certi.html"

	//path for download pdf
	outputPath := "storage/example.pdf"

	//html template data
	templateData := struct {
		Ten   string
		Khoa  string
		Diem  string
		Ngay  string
		TenTT string
	}{
		Ten:   "Hoang Kim Anh Duc",
		Khoa:  "Golang",
		Diem:  "700",
		Ngay:  "2022-09-09",
		TenTT: "BeEarning",
	}

	if err := r.ParseTemplate(templatePath, templateData); err == nil {
		ok, _ := r.GeneratePDF(outputPath)
		fmt.Println(ok, "pdf generated successfully")
	} else {
		fmt.Println(err)
	}
}
